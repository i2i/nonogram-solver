import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './components/Main';
import ErrorBoundary from './ErrorBoundary';

ReactDOM.render(
  <ErrorBoundary>
    <React.StrictMode>
      <Main />
    </React.StrictMode>
  </ErrorBoundary>,
  document.getElementById('root')
);
