import React from 'react';
import { connect } from 'react-redux';
import './ArgsButton.css';

const clickAction = ({args}, {scope}) => {
    const unassignedCells = scope.filter((cell) => cell.status === "unassigned");
    scope.length === unassignedCells.length && parseBlock(unassignedCells, args);
}

const parseBlock = (unassignedCells, args) => {
    args.map((arg, acc) => {
        if (arg > Math.floor(unassignedCells.length/2)) {
            console.log(`${acc}`);
            unassignedCells[3].status = "filled";
        }
        return "";
    })
}

const ArgsButton = ( {args, scope} ) => {
    return (
        <button className="args-button" onClick={() => clickAction(args={args},scope={scope})}>
            {args.join(", ")}
        </button>
    )
}

export default ArgsButton;