const Grid = props => {
    const colSize = +props.colArgs.length;
    const rowSize = +props.rowArgs.length;
    const rowsArray =[];
    const colsArray = [];
    const cellsArray = [];
    let cellCount = 0;
    for (let c = 0; c < colSize; c++) {
        for (let r = 0; r < rowSize; r++) {
            if (!rowsArray[r]){
                rowsArray[r] = [];
            }
            if (!colsArray[c]){
                colsArray[c] = [];
            }
            let newCell = {
                col: c,
                row: r,
                id: cellCount,
                status: "unassigned"
            }
            cellsArray.push(newCell);
            colsArray[c].push(newCell);
            rowsArray[r].push(newCell);
            cellCount++;
        }
    }
    return {cellsArray, colsArray, rowsArray}
}

export default Grid;