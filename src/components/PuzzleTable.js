import React from 'react';
import { useSelector } from 'react-redux';
import ClassNames from 'classnames';
import { Table } from 'reactstrap';
import './PuzzleTable.css';
import Grid from "./InitializeGrid";
import ArgsButton from './ArgsButton'


const PuzzleTable = ( {colArgs, rowArgs} ) => { 
    const gridArrays = Grid({colArgs, rowArgs})   
    return (
        <Table size="sm" bordered responsive>
            <thead>
                <tr>
                    <th></th>
                    {colArgs.map((col, u) => <th id={`col-args-${u}`} key={`col-args-${u}`} className={ClassNames("col-args","rotate")}>
                                <div>
                                    <span>
                                        <ArgsButton args={col} scope={gridArrays.colsArray[u]} />
                                    </span>
                                </div>
                            </th>)}
                </tr>
            </thead>
            <tbody>
                {
                    gridArrays.rowsArray.map((row, r) => 
                        <tr key={r} id={r}>
                            <th scope="row" id={`row-args-${r}`} key={`row-args-${r}`} className="row-args">
                                <ArgsButton args={rowArgs[r]} scope={gridArrays.rowsArray[r]} />
                            </th>
                            {row.map((cell) => <td key={cell.id} id={cell.id} className={ClassNames(`row-${cell.row}`, `col-${cell.col}`, `${cell.status}`)}></td>)}
                        </tr>
                    )
                }
            </tbody>
        </Table>
    )
}

export default PuzzleTable;