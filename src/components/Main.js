import React from 'react';
import { Provider } from 'react-redux';
import { Container } from 'reactstrap';
import PuzzleTable from './PuzzleTable';
import ColArgs from './ColArgs';
import RowArgs from './RowArgs';
import { store } from "../store";
import './Main.css';

const Main = () => (
    <Provider store = {store}>
        <Container>
            <h3>Nonogram Solver</h3>
            <PuzzleTable rows="10" cols="10" rowArgs={RowArgs} colArgs={ColArgs} />
        </Container>
    </Provider>
)

export default Main;