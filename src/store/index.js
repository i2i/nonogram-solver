import { createStore } from "redux";
// import { defaultState } from "../server/defaultState";

export const store = createStore(() => ({
  colArgs: [
    [1],[2],[1,6],[9],[6],[5],[5],[4],[3],[4]
  ],
  rowArgs: [
    [2],[1,1],[4],[2,1],[3,1],[8],[8],[7],[5],[3]
  ]
}));