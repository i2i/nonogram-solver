import { combineReducers } from 'redux';
const UPDATE_CELL = 'UPDATE_CELL';

export function updateCell(cell) {
    return {
        type: UPDATE_CELL,
        cell
    }
}

const defaultCells = {
        colArgs: [
            [1],[2],[1,6],[9],[6],[5],[5],[4],[3],[4]
          ],
          rowArgs: [
            [2],[1,1],[4],[2,1],[3,1],[8],[8],[7],[5],[3]
          ]
    }

function cells(state = defaultCells, action) {
    switch (action.type) {
        case UPDATE_CELL:
            const cell = state.find(c => c.id === "100");
            return [
                ...cells,
                {
                    ...cell,
                    status: "empty"
                }
            ];
        default:
            return state;
    }
}

const cellApp = combineReducers({
    cells
});

export default cellApp;

